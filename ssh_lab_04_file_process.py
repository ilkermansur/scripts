from netmiko import ConnectHandler

cisco_ios = {
    'device_type': 'cisco_ios',
    'host':   '192.168.71.204',
    'username': 'cisco',
    'password': 'cisco1',
    'port' : 22,          # optional, defaults to 22
}

net_connect = ConnectHandler(**cisco_ios)
output = net_connect.send_command('show ip int brief')

with open ('show_ip_int_brief.txt', 'w') as f:
    f.write(output)
    f.close()
    