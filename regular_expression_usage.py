import re

text = "ilker isminde  55 adet karakter ve 2 adet hece vardir."


numbers = re.findall(r'\d', text)
numbers2 = re.findall(r'\d{2}', text) # 2 digitlik ifadeleri bul

print(numbers2)

# print(numbers)

match01 = re.match('i',text) # Match fonksiyonu sadece ilk kelimenin eşleşmesi için kullanılır
print(match01.group())


search01 = re.search('5',text)
print (search01.group())


findall01 = re.findall('5',text)
print(findall01)


ip_address = '192.168.20.15 255.255.255.0'

ip_test = re.search('168',ip_address)
print(ip_test)

