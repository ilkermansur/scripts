from netmiko import ConnectHandler

cisco_ios = {
    'device_type': 'cisco_ios',
    'host':   'sandbox-iosxe-recomm-1.cisco.com',
    'username': 'developer',
    'password': 'lastorangerestoreball8876',
    'port' : 22,          # optional, defaults to 22
}

net_connect = ConnectHandler(**cisco_ios)

command_set = ['show ip int brief']

output = net_connect.send_config_set(command_set)

print(output)