from zeep import Client
from zeep.plugins import HistoryPlugin

# Call Manager AXL API WSDL URL
wsdl = 'https://192.168.91.33:8443/axl/AXLAPI.wsdl'

# API username and password
username = 'ccmadmin'
password = 'cucmlabbt'

# Initialize SOAP client
client = Client(wsdl=wsdl, username=username, password=password)

# Send an AXL query to retrieve phone serial number
query = """
    select name,serial from device where tkmodel='Cisco Unified IP Phone 7965G'
"""
response = client.service.executeSQLQuery(query)

# Print out the serial number of the first phone returned by the query
print(response['return']['row'][0]['serial'])
