from zeep import Client
from zeep.wsse.username import UsernameToken

# create a Zeep client for the CCM API
wsdl_url = 'https://192.168.91.33:8443/axl/'
username = 'ccmadmin'
password = 'cucmlabbt'
client = Client(wsdl=wsdl_url, wsse=UsernameToken(username, password))

# make a request to retrieve a specific phone
phone_name = 'SEP111122221001'
response = client.service.getPhone(name=phone_name)

# print the response
print(response)
